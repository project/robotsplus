<?php

/**
 * @file
 * Administrative page callbacks for the robotsplus module.
 */

/**
 * Administration settings form.
 */
function robotsplus_admin_settings() {
  $form['robotsplus'] = array(
    '#type' => 'fieldset',
    '#title' => t('List of directives'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['robotsplus']['robotsplus_allow'] = array(
    '#type' => 'textarea',
    '#title' => t('Allow'),
    '#default_value' => variable_get('robotsplus_allow', 'User-agent: *
Crawl-delay: 10'),
    '#cols' => 60,
    '#rows' => 8,
    '#weight' => 10,
    '#wysiwyg' => FALSE,
    '#description' => t('Block <em>Allow</em> placed first, and it can specify additional options such as : <em>User-agent: *</em> and <em>Crawl-delay: 10</em>'),
  );
  $form['robotsplus']['robotsplus_disallow'] = array(
    '#type' => 'textarea',
    '#title' => t('Disallow'),
    '#default_value' => variable_get('robotsplus_disallow', ''),
    '#cols' => 60,
    '#rows' => 8,
    '#weight' => 20,
    '#wysiwyg' => FALSE,
  );

  // Display descriptions of tokens
  if (module_exists('token')) {
    $patterns = token_get_list('global');
    $doc = "<dl>\n";
    foreach ($patterns['global'] as $name => $description) {
      $doc .= '<dt>['. $name .']</dt>';
      $doc .= '<dd>'. $description .'</dd>';
    }
    $doc .= "</dl>\n";
    $form['robotsplus']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 30,
    );
    $form['robotsplus']['token_help']['help_list'] = array(
      '#value' => $doc,
    );
  }
  
  return system_settings_form($form);
}